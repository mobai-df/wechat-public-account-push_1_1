export const config = {
    /**
     * 公众号配置
     */

    // 公众号APP_ID
    APP_ID: "wx79ab7d0847844a96",

    // 公众号APP_SECRET
    APP_SECRET: "fbefa40d263218512",

    // 模板消息id
    TEMPLATE_ID: "OZVxlTWiU-y9XxGkyppYZzD8vOpN07fAfRkzFgXnLvY",

    // 回调消息模板id, 用来看自己有没有发送成功的那个模板
    CALLBACK_TEMPLATE_ID: "wx79ab7d0847844a96",

    // 接收公众号消息的微信号，如果有多个，需要在[]里用英文逗号间隔，例如
    // [{
    //   name: "姣姣",
    //   id: "wx79ab7d0847844a96"
    // },
    // {
    //   name: "飞",
    //   id: "wx79ab7d0847844a96"
    // }]
     
    // 接收成功回调消息的微信号，（一般来说只填自己的微信号, name填不填无所谓）
    CALLBACK_USERS: [
      {name: '自己',id: "wx79ab7d0847844a96"}, 
    ],
     
    /**
     * 信息配置
     */

    /** 天气相关 */

    // 所在省份
    PROVINCE: "江苏",
    // 所在城市
    CITY: "苏州市虎丘区",}